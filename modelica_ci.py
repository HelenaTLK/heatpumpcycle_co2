import dymola_testing as dt

# cmd --> docker run --mac-address=34:f3:9a:6c:65:0b -v=C:\Git\HeatPumpCycle:/test/src modelica-ci:2020 

print('\n===========================================================================\n')
testClass = dt.ModelicaPackage('/usr/local/bin/Modelica/Modelica 3.2.3') #load all depending packages, you need to make sure they are existing in modelica-ci repo
testClass.load_modelica_package('/usr/local/bin/Modelica/TILMedia 3.9.0')    
testClass.load_modelica_package('/usr/local/bin/Modelica/TIL 3.9.0')
testClass.load_modelica_package('./src/Modelica/TemperatureControl') # e.g. <path> = ./src/Modelica/OneZoneModel (note: current path is /test)

testClass.build_fmu('HeatPumpCycle_CO2_pascal_f0', # e.g. <model> = OneZoneModel.FMU.THEDA_Online
    modelName='HeatPumpCycle_CO2_FMU_1',
    fmiVersion='2',
    fmiType='csSolver', # Dymola solver
    includeSource=False)

testClass.close_dymola_interface()

#%% Checking exported FMU
dt.check_all_fmu(True,True)   

#%% Copying FMU to local directory
dt.copy_all_fmu()
