within ;
package HeatPumpCycle_CO2
annotation (uses(
    Modelica(version="3.2.3"),
    TIL(version="3.9.0 HD"),
    TILMedia(version="3.9.0")));
end HeatPumpCycle_CO2;
