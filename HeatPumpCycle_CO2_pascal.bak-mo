within HeatPumpCycle_CO2;
model HeatPumpCycle_CO2_pascal "Heat Pump Cycle CO2"

  parameter Real supply_temperature_setPoint = 65;           // degC
  parameter Real return_temperature = 10;                    // degC
  parameter Real compressor_speed = 50;                      // Hz
  parameter Real compressor_displacement = 10;               // ml
  parameter Real ambient_temperature = 10;                   // degC
  parameter Real ambient_phi = 10;                           // %
  parameter Boolean use_heatRecovery = true;

  parameter Real thermalResistance_heatRecovery = if use_heatRecovery then 1e-6 else 1e6;

  TIL.LiquidComponents.Boundaries.Boundary waterSource(
    V_flowFixed=-3e-5,
    TFixed=273.15 + return_temperature,
    m_flowFixed=-5e-1,
    boundaryType="p",
    pFixed=300000) annotation (Placement(transformation(extent={{-84,50},{-76,
            70}}, rotation=0)));
  TIL.LiquidComponents.Boundaries.Boundary waterSink(boundaryType="p", pFixed=
        300000) annotation (Placement(transformation(extent={{36,50},{44,70}},
          rotation=0)));
  inner TIL.SystemInformationManager sim(
    redeclare replaceable TILMedia.VLEFluidTypes.TILMedia_CO2 vleFluidType1,
    redeclare replaceable TILMedia.GasTypes.VDI4670_MoistAir gasType1,
    redeclare replaceable TILMedia.LiquidTypes.TILMedia_Water liquidType1)
                                                               annotation (
      Placement(transformation(extent={{140,80},{160,100}},rotation=0)));

  TIL.LiquidComponents.Pumps.SimplePump pump(
    presetVariableType="V_flow",
      m_flowFixed=0.03,
    use_volumeFlowRateInput=true,
    V_flowFixed(displayUnit="m3/s") = 3.3333333333333e-5)
                        annotation (Placement(transformation(
        origin={-30,60},
        extent={{-8,-8},{8,8}},
        rotation=270)));

  TIL.VLEFluidComponents.PressureStateElements.PressureState pressureState_hp(pressureStateID=1, pInitial=9000000)
    annotation (Placement(transformation(extent={{44,38},{56,50}}, rotation=0)));

  TIL.HeatExchangers.TubeAndTube.VLEFluidLiquid.ParallelFlowHX gasCooler(
    pressureStateID=1,
    nCells=10,
    redeclare TIL.HeatExchangers.TubeAndTube.Geometry.TubeAndTubeGeometry hxGeometry(length_a=20, length_b=20),
    redeclare model HeatTransferModel_a =
        TIL.HeatExchangers.TubeAndTube.TransportPhenomena.TubeSideHeatTransfer.ConstantAlphaA
        (                                                                                      constantAlphaA=2000),
    redeclare model PressureDropModel_a =
        TIL.HeatExchangers.TubeAndTube.TransportPhenomena.TubeSidePressureDrop.ZeroPressureDrop,
    redeclare model HeatTransferModel_b =
        TIL.HeatExchangers.TubeAndTube.TransportPhenomena.TubeSideHeatTransfer.ConstantAlphaA
        (                                                                                      constantAlphaA=2000),
    redeclare model PressureDropModel_b =
        TIL.HeatExchangers.TubeAndTube.TransportPhenomena.TubeSidePressureDrop.ZeroPressureDrop,
    heatCapacityInputType="m, solid",
    thermalResistanceInputType="R",
    heatCapacity=1,
    solidMass=6,
    thermalResistance=6e-5,
    redeclare model WallMaterial = TILMedia.SolidTypes.TILMedia_Copper,
    hInitialVLEFluid_a=350e3,
    initVLEFluid_a="linearEnthalpyDistribution",
    hInitialVLEFluid_a_CellN=300e3,
    hInitialVLEFluid_a_Cell1=500e3) annotation (Placement(transformation(extent={{20,38},{-8,66}}, rotation=0)));

  TIL.VLEFluidComponents.Sensors.StatePoint statePoint1(stateViewerIndex=1)
    annotation (Placement(transformation(extent={{68,50},{76,58}}, rotation=0)));
  TIL.VLEFluidComponents.Sensors.StatePoint statePoint2(stateViewerIndex=2)
    annotation (Placement(transformation(extent={{-94,34},{-86,42}}, rotation=0)));
  TIL.VLEFluidComponents.Valves.OrificeValve expansionValve(
      use_effectiveFlowAreaInput=false, effectiveFlowAreaFixed=0.4e-6)
    annotation (Placement(transformation(
        origin={-80,-30},
        extent={{-8,-4},{8,4}},
        rotation=270)));
  TIL.HeatExchangers.FinAndTube.MoistAirVLEFluid.ParallelFlowHX evaporator(
    pressureStateID=2,
    nCells=5,
    redeclare model TubeSideHeatTransferModel =
        TIL.HeatExchangers.FinAndTube.TransportPhenomena.TubeSideHeatTransfer.ConstantAlphaA
        (                                                                                     constantAlphaA=2000),
    redeclare model FinSideHeatTransferModel =
        TIL.HeatExchangers.FinAndTube.TransportPhenomena.FinSideHeatTransfer.ConstantAlphaA
        (                                                                                    constantAlphaA=2000),
    m_flowMoistAirStart=0.7,
    initVLEFluid="linearEnthalpyDistribution",
    m_flowVLEFluidStart=-0.03,
    TInitialWall=273.15 + 5,
    redeclare model WallHeatConductionModel =
        TIL.HeatExchangers.FinAndTube.TransportPhenomena.WallHeatTransfer.GeometryBasedConduction,
    redeclare model FinEfficiencyModel =
        TIL.HeatExchangers.FinAndTube.TransportPhenomena.FinEfficiency.ConstFinEfficiency,
    redeclare model TubeSidePressureDropModel =
        TIL.HeatExchangers.FinAndTube.TransportPhenomena.TubeSidePressureDrop.ZeroPressureDrop,
    redeclare model FinSidePressureDropModel =
        TIL.HeatExchangers.FinAndTube.TransportPhenomena.FinSidePressureDrop.ZeroPressureDrop,
    flagActivateDynWaterBalance=true,
    redeclare model WallMaterial = TILMedia.SolidTypes.TILMedia_Copper,
    redeclare model FinMaterial = TILMedia.SolidTypes.TILMedia_Aluminum,
    hxGeometry(nParallelTubes=8),
    hInitialVLEFluid_Cell1=430e3,
    hInitialVLEFluid_CellN=280e3,
    pVLEFluidStart=300000) annotation (Placement(transformation(extent={{4,-72},{-24,-44}}, rotation=0)));

  TIL.VLEFluidComponents.PressureStateElements.PressureState pressureState_lp(pressureStateID=2, pInitial=3500000)
    annotation (Placement(transformation(extent={{36,-56},{48,-44}}, rotation=0)));
  TIL.GasComponents.Boundaries.Boundary gasSource(
    use_temperatureInput=false,
    phiFixed=ambient_phi,
    TFixed=273.15 + ambient_temperature,
    boundaryType="V_flow",
    V_flowFixed=-1,
    streamVariablesInputTypeConcentration="phi") annotation (Placement(
        transformation(extent={{24,-76},{16,-56}}, rotation=0)));
  TIL.GasComponents.Boundaries.Boundary gasSink(
    use_temperatureInput=false,
    V_flowFixed=-1,
    phiFixed=10,
    boundaryType="p",
    TFixed=273.15 + 10,
    streamVariablesInputTypeConcentration="phi") annotation (Placement(
        transformation(extent={{-44,-76},{-36,-56}}, rotation=0)));
  TIL.VLEFluidComponents.Sensors.StatePoint statePoint3(stateViewerIndex=3)
    annotation (Placement(transformation(
        origin={-90,-10},
        extent={{-3.99999,-4.00004},{4.00002,3.99996}},
        rotation=0)));
  TIL.VLEFluidComponents.Separators.Separator separator(pressureStateID=2,
      V=0.6e-3) annotation (Placement(transformation(extent={{54,-64},{66,-44}},
          rotation=0)));
  TIL.VLEFluidComponents.Compressors.EffCompressor compressor(
    use_mechanicalPort=true,
    displacement(displayUnit="l") = compressor_displacement/1e6,
    nFixed=50,
    volumetricEfficiency=0.8,
    isentropicEfficiency=0.8,
    effectiveIsentropicEfficiency=0.8) annotation (Placement(transformation(
          extent={{72,22},{88,38}}, rotation=0)));

  TIL.VLEFluidComponents.Sensors.StatePoint statePoint4(stateViewerIndex=4)
    annotation (Placement(transformation(
        extent={{-4,-4},{4,4}},
        rotation=0,
        origin={-40,-42})));
  TIL.VLEFluidComponents.Sensors.StatePoint statePoint5(stateViewerIndex=5)
    annotation (Placement(transformation(extent={{24,-46},{32,-38}},rotation=0)));
  TIL.VLEFluidComponents.Sensors.StatePoint statePoint6(stateViewerIndex=6)
    annotation (Placement(transformation(extent={{86,-46},{94,-38}}, rotation=0)));
  TIL.VLEFluidComponents.Tubes.Tube tube1(
    redeclare TIL.VLEFluidComponents.Tubes.Geometry.Tube6x1 tubeGeometry(length=1),
    nCells=4,
    redeclare model TubeSideHeatTransferModel =
        TIL.VLEFluidComponents.Tubes.TransportPhenomena.HeatTransfer.ConstantAlphaA
        (                                                                            constantAlphaA=80),
    redeclare model WallHeatConductionModel =
        TIL.VLEFluidComponents.Tubes.TransportPhenomena.WallHeatTransfer.GeometryBasedConduction,
    redeclare model PressureDropModel =
        TIL.VLEFluidComponents.Tubes.TransportPhenomena.PressureDrop.ZeroPressureDrop,
    redeclare model WallMaterial = TILMedia.SolidTypes.TILMedia_Copper,
    enableHeatPorts=true,
    wallCellStateType="state north",
    m_flowStart=0.03,
    hInitialVLEFluid=300e3,
    initVLEFluid="linearEnthalpyDistribution",
    hInitialVLEFluid_Cell1=300e3,
    hInitialVLEFluid_CellN=280e3,
    pStart=10000000,
    fixedTInitialWall=false)
    annotation (Placement(transformation(
        extent={{-8,-2},{8,2}},
        rotation=270,
        origin={-80,14})));

  TIL.VLEFluidComponents.Tubes.Tube tube2(
    redeclare TIL.VLEFluidComponents.Tubes.Geometry.Tube6x1 tubeGeometry(length=1),
    nCells=4,
    redeclare model TubeSideHeatTransferModel =
        TIL.VLEFluidComponents.Tubes.TransportPhenomena.HeatTransfer.ConstantAlphaA
        (                                                                            constantAlphaA=80),
    redeclare model WallHeatConductionModel =
        TIL.VLEFluidComponents.Tubes.TransportPhenomena.WallHeatTransfer.GeometryBasedConduction,
    redeclare model PressureDropModel =
        TIL.VLEFluidComponents.Tubes.TransportPhenomena.PressureDrop.ZeroPressureDrop,
    redeclare model WallMaterial = TILMedia.SolidTypes.TILMedia_Copper,
    enableHeatPorts=true,
    wallCellStateType="state north",
    pressureStateID=2,
    m_flowStart=-0.03,
    hInitialVLEFluid=430e3,
    initVLEFluid="linearEnthalpyDistribution",
    hInitialVLEFluid_Cell1=450e3,
    hInitialVLEFluid_CellN=430e3,
    pStart=3000000)
    annotation (Placement(transformation(
        extent={{-8,2},{8,-2}},
        rotation=270,
        origin={80,-20})));

  TIL.VLEFluidComponents.Sensors.StatePoint statePoint7(stateViewerIndex=7)
    annotation (Placement(transformation(
        extent={{-4,-4},{4,4}},
        rotation=0,
        origin={90,8})));
  TIL.VLEFluidComponents.Sensors.StatePoint statePoint8(stateViewerIndex=8)
    annotation (Placement(transformation(
        extent={{-4,-4},{4,4}},
        rotation=0,
        origin={88,50})));
  TIL.OtherComponents.Mechanical.RotatoryBoundary rotatoryBoundary(use_nInput=false, nFixed=compressor_speed)
    annotation (Placement(transformation(extent={{98,20},{90,40}})));
  Modelica.Blocks.Sources.RealExpression COP_calc(y=-gasCooler.summary.Q_flow/compressor.shaftPower)
    annotation (Placement(transformation(extent={{130,50},{150,70}})));
  Modelica.Blocks.Interfaces.RealOutput COP annotation (Placement(transformation(extent={{160,50},{180,70}})));
  TIL.OtherComponents.Thermal.HeatResistor heatResistor[4](inputType="R", thermalResistance=thermalResistance_heatRecovery)
                                                           annotation (Placement(transformation(extent={{-8,-10},{8,-6}})));
  Modelica.Blocks.Sources.RealExpression compressor_shaftPower_calc(y=compressor.shaftPower/1000)
    annotation (Placement(transformation(extent={{130,30},{150,50}})));
  Modelica.Blocks.Interfaces.RealOutput compressor_shaftPower
    annotation (Placement(transformation(extent={{160,30},{180,50}})));
  TIL.OtherComponents.Controllers.PIController
                                           PI_T_supply(
    invertFeedback=true,
    yMin=1e-20,
    yMax=0.0002,
    k=2e-6,
    Ti=20,
    controllerType="PI",
    initType="initialOutput",
    yInitial=3.3333333333333e-5,
    activationTime=0)
                     annotation (Placement(transformation(
        extent={{-6,6},{6,-6}},
        rotation=180,
        origin={32,86})));
  Modelica.Blocks.Sources.RealExpression T_supply_setPoint(y=supply_temperature_setPoint + 273.15)
    annotation (Placement(transformation(extent={{72,76},{52,96}})));
  Modelica.Blocks.Sources.RealExpression pump_Vflow_calc(y=pump.summary.V_flow_lmin_A)
    annotation (Placement(transformation(extent={{130,10},{150,30}})));
  Modelica.Blocks.Interfaces.RealOutput pump_Vflow annotation (Placement(transformation(extent={{160,10},{180,30}})));
  TIL.LiquidComponents.Sensors.Sensor_T sensor_T_supply annotation (Placement(transformation(extent={{28,66},{36,74}})));
  Modelica.Blocks.Sources.RealExpression pressure_HP_calc(y=sensor_HP.sensorValue*1e-5)
    annotation (Placement(transformation(extent={{130,-10},{150,10}})));
  Modelica.Blocks.Interfaces.RealOutput pressure_HP annotation (Placement(transformation(extent={{160,-10},{180,10}})));
  TIL.VLEFluidComponents.Sensors.Sensor_p sensor_HP annotation (Placement(transformation(extent={{-94,48},{-86,56}})));
  TIL.VLEFluidComponents.Sensors.Sensor_p sensor_LP annotation (Placement(transformation(extent={{-56,-46},{-48,-38}})));
  Modelica.Blocks.Sources.RealExpression pressure_LP_calc(y=sensor_LP.sensorValue*1e-5)
    annotation (Placement(transformation(extent={{130,-30},{150,-10}})));
  Modelica.Blocks.Interfaces.RealOutput pressure_LP annotation (Placement(transformation(extent={{160,-30},{180,-10}})));
  Modelica.Blocks.Sources.RealExpression heatRecovery_Qflow_calc(y=tube2.summary.Q_flow_vle)
    annotation (Placement(transformation(extent={{130,-50},{150,-30}})));
  Modelica.Blocks.Interfaces.RealOutput heatRecovery_Qflow
    annotation (Placement(transformation(extent={{160,-50},{180,-30}})));
  TIL.VLEFluidComponents.Sensors.Sensor_superheating sensor_superheating_LP
    annotation (Placement(transformation(extent={{66,4},{74,12}})));
  Modelica.Blocks.Sources.RealExpression superheating_LP_calc(y=sensor_superheating_LP.sensorValue)
    annotation (Placement(transformation(extent={{130,-70},{150,-50}})));
  Modelica.Blocks.Interfaces.RealOutput superheating_LP
    annotation (Placement(transformation(extent={{160,-70},{180,-50}})));
  Modelica.Blocks.Sources.RealExpression temperature_supply_calc(y=sensor_T_supply.sensorValue - 273.15)
    annotation (Placement(transformation(extent={{130,-90},{150,-70}})));
  Modelica.Blocks.Interfaces.RealOutput temperature_supply
    annotation (Placement(transformation(extent={{160,-90},{180,-70}})));
equation
  connect(evaporator.portB_gas, gasSink.port) annotation (Line(
      points={{-24,-66},{-32,-66},{-40,-66}},
      color={255,153,0},
      thickness=0.5));
  connect(compressor.portB, pressureState_hp.portA) annotation (Line(
      points={{80,38},{80,44},{56,44}},
      color={153,204,0},
      thickness=0.5));
  connect(statePoint1.sensorPort, compressor.portB) annotation (Line(
      points={{72,50},{72,44},{80,44},{80,38}},
      color={153,204,0},
      thickness=0.5));
  connect(statePoint3.sensorPort, expansionValve.portA) annotation (Line(
      points={{-90,-14},{-90,-18},{-80,-18},{-80,-22}},
      color={153,204,0},
      thickness=0.5,
      smooth=Smooth.None));
  connect(statePoint6.sensorPort, separator.portGas) annotation (Line(
      points={{90,-46},{90,-50},{65,-50}},
      color={153,204,0},
      thickness=0.5,
      smooth=Smooth.None));

  connect(gasCooler.portA_a, pressureState_hp.portB) annotation (Line(
      points={{20,44},{44,44}},
      color={153,204,0},
      thickness=0.5,
      smooth=Smooth.None));
  connect(gasCooler.portA_b, waterSink.port) annotation (Line(
      points={{20,60},{40,60}},
      color={0,170,238},
      thickness=0.5,
      smooth=Smooth.None));
  connect(gasCooler.portB_b, pump.portB) annotation (Line(
      points={{-8,60},{-22,60}},
      color={0,170,238},
      thickness=0.5,
      smooth=Smooth.None));
  connect(statePoint2.sensorPort, gasCooler.portB_a) annotation (Line(
      points={{-90,34},{-90,30},{-80,30},{-80,44},{-8,44}},
      color={153,204,0},
      thickness=0.5,
      smooth=Smooth.None));
  connect(gasSource.port, evaporator.portA_gas) annotation (Line(
      points={{20,-66},{4,-66}},
      color={255,153,0},
      thickness=0.5,
      smooth=Smooth.None));
  connect(evaporator.portA_vle, pressureState_lp.portB) annotation (Line(
      points={{4,-50},{36,-50}},
      color={153,204,0},
      thickness=0.5,
      smooth=Smooth.None));
  connect(statePoint4.sensorPort, evaporator.portB_vle) annotation (Line(
      points={{-40,-46},{-40,-50},{-24,-50}},
      color={153,204,0},
      thickness=0.5,
      smooth=Smooth.None));
  connect(evaporator.portA_vle, statePoint5.sensorPort) annotation (Line(
      points={{4,-50},{28,-50},{28,-46}},
      color={153,204,0},
      thickness=0.5,
      smooth=Smooth.None));
  connect(expansionValve.portB, evaporator.portB_vle) annotation (Line(
      points={{-80,-38},{-80,-50},{-24,-50}},
      color={153,204,0},
      thickness=0.5,
      smooth=Smooth.None));
  connect(expansionValve.portA, tube1.portB) annotation (Line(
      points={{-80,-22},{-80,6}},
      color={153,204,0},
      thickness=0.5,
      smooth=Smooth.None));
  connect(compressor.portA, tube2.portA) annotation (Line(
      points={{80,22},{80,-12}},
      color={153,204,0},
      thickness=0.5,
      smooth=Smooth.None));
  connect(separator.portGas, tube2.portB) annotation (Line(
      points={{65,-50},{80,-50},{80,-28}},
      color={153,204,0},
      thickness=0.5,
      smooth=Smooth.None));
  connect(pressureState_lp.portA, separator.portInlet) annotation (Line(
      points={{48,-50},{55,-50}},
      color={153,204,0},
      thickness=0.5,
      smooth=Smooth.None));
  connect(statePoint7.sensorPort, tube2.portA) annotation (Line(
      points={{90,4},{90,0},{80,0},{80,-12}},
      color={153,204,0},
      thickness=0.5,
      smooth=Smooth.None));
  connect(gasCooler.portB_a, tube1.portA) annotation (Line(
      points={{-8,44},{-8,44},{-80,44},{-80,42},{-80,42},{-80,22},{-80,22}},
      color={153,204,0},
      thickness=0.5,
      smooth=Smooth.None));
  connect(statePoint8.sensorPort, pressureState_hp.portA)
    annotation (Line(
      points={{88,46},{88,42},{80,42},{80,44},{56,44}},
      color={153,204,0},
      thickness=0.5));
  connect(compressor.rotatoryFlange, rotatoryBoundary.rotatoryFlange)
    annotation (Line(
      points={{88,30},{94,30}},
      color={135,135,135},
      thickness=0.5));
  connect(COP_calc.y, COP) annotation (Line(points={{151,60},{170,60}},   color={0,0,127}));
  connect(waterSource.port, pump.portA)
    annotation (Line(
      points={{-80,60},{-38,60}},
      color={0,170,238},
      thickness=0.5));
  connect(tube1.heatPort, heatResistor.heatPortA)
    annotation (Line(
      points={{-78,14},{-44,14},{-44,-8},{-8,-8}},
      color={204,0,0},
      thickness=0.5));
  connect(heatResistor.heatPortB, tube2.heatPort)
    annotation (Line(
      points={{8,-8},{44,-8},{44,-20},{78,-20}},
      color={204,0,0},
      thickness=0.5));
  connect(compressor_shaftPower_calc.y, compressor_shaftPower)
    annotation (Line(points={{151,40},{170,40}},   color={0,0,127}));
  connect(PI_T_supply.y, pump.V_flow_in) annotation (Line(points={{25.6,86},{-30,86},{-30,70}}, color={0,0,127}));
  connect(T_supply_setPoint.y, PI_T_supply.u_s) annotation (Line(points={{51,86},{37.6,86}}, color={0,0,127}));
  connect(pump_Vflow_calc.y, pump_Vflow) annotation (Line(points={{151,20},{170,20}}, color={0,0,127}));
  connect(sensor_T_supply.port, waterSink.port)
    annotation (Line(
      points={{32,66},{32,60},{40,60}},
      color={0,170,238},
      thickness=0.5));
  connect(PI_T_supply.u_m, sensor_T_supply.sensorValue) annotation (Line(points={{32,80.2},{32,72}}, color={0,0,127}));
  connect(pressure_HP_calc.y, pressure_HP) annotation (Line(points={{151,0},{170,0}}, color={0,0,127}));
  connect(sensor_HP.port, gasCooler.portB_a) annotation (Line(
      points={{-90,48},{-90,42},{-80,42},{-80,44},{-8,44}},
      color={153,204,0},
      thickness=0.5));
  connect(sensor_LP.port, evaporator.portB_vle)
    annotation (Line(
      points={{-52,-46},{-52,-50},{-24,-50}},
      color={153,204,0},
      thickness=0.5));
  connect(pressure_LP_calc.y, pressure_LP) annotation (Line(points={{151,-20},{170,-20}}, color={0,0,127}));
  connect(heatRecovery_Qflow_calc.y, heatRecovery_Qflow)
    annotation (Line(points={{151,-40},{170,-40}}, color={0,0,127}));
  connect(superheating_LP_calc.y, superheating_LP) annotation (Line(points={{151,-60},{170,-60}}, color={0,0,127}));
  connect(sensor_superheating_LP.port, tube2.portA)
    annotation (Line(
      points={{70,4},{70,0},{80,0},{80,-12}},
      color={153,204,0},
      thickness=0.5));
  connect(temperature_supply_calc.y, temperature_supply)
    annotation (Line(points={{151,-80},{170,-80}}, color={0,0,127}));
  annotation (
    experiment(StopTime=3000),
    experimentSetupOutput,
    Documentation(info="<html>
        <p>
        This CO2 cycle represents a typical air-water-heat pump for a building.
        The R744 heat pump has a tube and tube heat exchanger as gas cooler on the high pressure side.
        The gas cooler heats up the water for the building.
        On the low pressure level there is a fin and tube heat exchanger used as evaporator.
        The evaporator stands outside the building and takes the heat from the ambient air.
        This heat pump also has an internal heat exchanger, which is realized with two tubes and a heat port connection.
        </p>
        </html>"),
    Diagram(coordinateSystem(extent={{-100,-100},{160,100}})),
    Icon(coordinateSystem(extent={{-100,-100},{100,100}})));
end HeatPumpCycle_CO2_pascal;
